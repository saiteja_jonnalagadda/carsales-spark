
from pyspark.sql.functions import *
import pyspark
from pyspark.sql import SQLContext
from pyspark.conf import SparkConf
from pyspark.sql import SparkSession
from pyspark.sql import Row
from pyspark.sql.types import *
import os


def main():
    spark = SparkSession.builder.config("test", "test").appName("read file in google drive").enableHiveSupport().getOrCreate()
    sc = spark.sparkContext
    sqlContext = SQLContext(sparkContext=spark.sparkContext, sparkSession=spark)
    df = spark.read.format("csv").option("header", "true").option("delimiter", ";").load("data.csv")
    df.createOrReplaceTempView("cars_sales")
    df.show()

    # cars price below 70000 and used not more than 10000 kms

    less_price_kms = "select * from cars_sales where kms < 10000 and price < 70000"
    sales_result = spark.sql(less_price_kms)
    sales_result.show()
    sales_result.toPandas().to_csv('sales_result.csv', header=True, index=False)

    # count of cars by fuel_type

    fuel_type = "select fuel_type,count(*) as number_of_cars from cars_sales group by fuel_type "
    fuel_type_result = spark.sql(fuel_type)
    fuel_type_result.show()
    fuel_type_result.toPandas().to_csv('fuel_type_result.csv', header=True, index=False)

    # number of cars by make

    make = "select make,count(*) as number_of_cars from cars_sales group by make"
    make_result = spark.sql(make)
    make_result.show()
    make_result.toPandas().to_csv('make_result.csv', header=True, index=False)

    # number of cars with power > 300 and months_old < 24

    power_months = "select * from cars_sales where power > 300 and months_old < 24"
    power_months_reslut = spark.sql(power_months)
    power_months_reslut.show()
    power_months_reslut.toPandas().to_csv('power_months_reslut.csv', header=True, index=False)




if __name__ == '__main__':
    main()